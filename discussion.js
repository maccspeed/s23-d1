// CRUD Operations
/*
	create -
	read -
	update -
	delete -
*/


// Inserting Documents (Create)

// Syntax: db.collectionName.insertOne({object});
// JavaScript: object.object.method({object});

db.users.insertOne({
    "firstName": "Jane",
    "lastName": "Doe",
    "age": 21,
    "contact": {
        "phone": "09196543210",
        "email": "janedoe@mail.com"
    },
    "courses": ["CSS", "JavaScript", "Python"],
    "department": "none"
});

// Insert Many
// Syntax: db.collectionName.insertMany([{objectA},{objectB}]);

db.users.insertMany([
{
    "firstName": "Stephen",
    "lastName": "Hawking",
    "age": 76,
    "contact": {
        "phone": "09179876543",
        "email": "stephenhawking@mail.com"
    },
    "courses": ["React", "PHP", "Python"],
    "department": "none"
},
{
    "firstName": "Neil",
    "lastName": "Armstrong",
    "age": 82,
    "contact": {
        "phone": "09061234587",
        "email": "neilarmstrong@mail.com"
    },
    "courses": ["React", "Laravel", "SASS"],
    "department": "none"
}
]);

// Finding documetns (Read) Operation
/* Find Syntax:
	db.collectionName.find({});
	db.collectionName.find({field: value});
*/

db.users.find();
db.users.find({"lastName": "Doe"});
db.users.find({"lastName": "Doe", "age": 25}).pretty();

// Updating Documents (Update) Operations
// Syntax: db.collection.updateOne({criteria}, {$set: {field: value}});

// Insert Sample document
db.users.insertOne({
    "firstName": "Test",
    "lastName": "Test",
    "age": 0,
    "contact": {
        "phone": "00000000000",
        "email": "test@mail.com"
    },
    "courses": [],
    "department": "none"
});

// Update One document
db.users.updateOne({"firstName": "Test"},
{$set:
    {
        "firstName": "Bill",
        "lastName": "Gates",
        "age": 65,
        "contact": {
            "phone": "09170123465",
            "email": "bill@mail.com"
        },
        "courses": ["PHP", "Laravel", "HTML"],
        "department": "none"
    }
}
);

// Update Multiple documents
db.users.updateMany({"department": "none"},
{$set:
    {
        "department": "HR"
    }
}
);

// Deleting Documents (Delete) Operation
// Syntax: db.collectionname.deleteOne({criteria}); - delete single document

// Create a single document with one field
db.users.insert({
    "firstName": "test"
});

// Delete One document
db.users.deleteOne({
    "firstName": "test"
});


// Update many documents
db.users.updateMany({"lastName": "Doe"},
{$set:
    {"department": "Operations"}
}
);

// Delete many documents
db.users.deleteMany(
    {"department": "Operations"}
);

// Advance Query

// Query and embedded document
db.users.find({
	"contact": {
		"phone": "09170123465",
		"email": "bill@mail.com"
	}
});

// Querying an Array without a specific order of elements
db.users.find({ "courses": {$all: ["React", "Python"]} });